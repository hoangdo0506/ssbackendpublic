/**
 * Created by hoangdo on 9/16/2017.
 */


const ShopItemDetail = require('../../models').shop_item_detail;
const ItemOption = require('../../models').item_option;

module.exports ={
    create(req, res){
        return ShopItemDetail
            .create({
                    local_item_name: req.body.local_item_name,
                    local_item_code: req.body.local_item_code,
                    local_item_desc: req.body.local_item_desc,
                    item_price: req.body.item_price,
                    item_unit: req.body.item_unit,
                    local_photo: req.body.local_photo,
                local_photo2: req.body.local_photo2,
                local_photo3: req.body.local_photo3,
                local_photo4: req.body.local_photo4,
                local_photo5: req.body.local_photo5,
                shopId: req.body.shopId,
                itemOptionId: req.body.itemOptionId,
                itemId: req.body.itemId

            })
            .then(shopItem => res.status(201).send(shopItem))
            .catch (error => res.status(500).send(error));
    },
    list(req,res){
        return ShopItemDetail
            .findAll({
                include: [{
                    all:true
                }]
            })
            .then(shopItem => res.status(200).send(shopItem))
            .catch(error => res.status(500).send(error));
    },
    destroy(req, res){
        return ShopItemDetail
            .findById(req.params.shopItemId)
            .then(item => {
                if(!item){
                    return res.status(400).send({
                        message: 'Item not found'
                    });
                }
                return item
                    .destroy()
                    .then(()=> res.status(204).send({
                        message: 'Item Delete Successfully.'
                    }))
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => res.status(400).send(error));

    },
}