/**
 * Created by hoangdo on 9/16/2017.
 */
const Account = require('../../models').user;
const Contact = require('../../models').contact;

module.exports = {
    create(req,res){
         return Contact
        .create({
            email: req.body.email,
            email2: req.body.email2,
            name: req.body.name,
            name2: req.body.name2,
            hotline: req.body.hotline,
            office_phone: req.body.office_phone,
            office_phone2: req.body.office_phone2
        })
        .then(contact => {
            return Account
                .create({
                    username: req.body.username,
                    password: req.body.password,
                    googleID: req.body.googleID,
                    facebookID: req.body.facebookID,
                    avatar: req.body.avatar,
                    type: req.body.type,
                    role: req.body.role,
                    contactId: contact.id
                })
                .then(user => res.status(201).send(user))
                .catch(error => res.status(500).send(error));
        })
        .catch(error => res.status(500).send(error));
    },
    list(req, res){
        return Account
            .findAll({
                include: {
                   model: Contact
                }
            })
            .then(user => res.status(201).send(user))
            .catch(error => res.status(500).send(error));
    },
    retrieve(req, res){
        return Account
            .findById(req.params.userId ,{
                include: {
                    model: Contact
                }
            })
            .then(user => {
                if(!user){
                    return res.status(404).send({
                        message: 'User not found'
                    })
                }
                return res.status(201).send(user);
            })

            .catch(error => res.status(500).send(error));
    }

};