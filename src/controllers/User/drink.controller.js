/**
 * Created by hoangdo on 9/16/2017.
 */
const Item = require('../../models').item;
const ItemOption =require('../../models').item_option;
const ShopItemDetails = require('../../models').shop_item_detail;
const ShopItem =require('../../models').shop;
module.exports = {

    list(req,res){
        return ItemOption
            .findAll({
                include: [{
                    model: ShopItemDetails,
                    as: 'shop_item_details'
                }],
            })
            .then(item =>res.status(201).send(item))
            .catch(err =>res.status(400).send(err));
    },
    retrieve(req,res){
        return Item
            .findById(req.params.itemId ,{

                include: [{
                    model: ItemOption,
                    as: 'item_options'
                }],
            })
            .then(item =>{
                if(!item){
                    return res.status(404).send({
                        message: 'Item not found'
                    });
                }
                return res.status(200).send(item);
            })
            .catch(error => res.status(400).send(error));
    },
    // retrieve(req,res){
    //     return ShopItem
    //         .findById(req.params.shopId ,{
    //
    //             include: [{
    //                 model: ShopItemDetails,
    //                 as: 'shop_item_details'
    //             }],
    //         })
    //         .then(item =>{
    //             if(!item){
    //                 return res.status(404).send({
    //                     message: 'Item not found'
    //                 });
    //             }
    //             return res.status(200).send(item);
    //         })
    //         .catch(error => res.status(400).send(error));
    // },


};