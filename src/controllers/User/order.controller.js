/**
 * Created by hoangdo on 9/16/2017.
 */

const Order = require('../../models').order;
const OrderDetail = require('../../models').order_detail;

module.exports ={
    create(req,res){
        return Order
            .create({
                notes: req.body.notes,
                address: req.body.address,
                mobile: req.body.mobile,
                mobile2: req.body.mobile2,
                cash_type: req.body.cash_type,
                total: req.body.total,
                sub_total:req.body.sub_total,
                tax: req.body.tax,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                ship: req.body.ship,
                trackingNumber: req.body.trackingNumber,
                status: req.body.status,
                paymentId: req.body.paymentId,
                userId: req.params.userId
            })
            .then(order =>{
                return OrderDetail
                    .create({
                        quantity: req.body.quantity,
                        sum: req.body.sum,
                        image: req.body.image,
                        name: req.body.name,
                        code: req.body.code,
                        orderId: order.id
                    })
                    .then(order => res.status(200).send(order))
                    .catch (error => res.status(500).send(error))
            })

            .catch (error => res.status(500).send(error));
    },
    update(req, res){
        return Order
            .findById(req.params.orderId ,{
                where: req.params.userId
            })
            .then(order =>{
                if(!order){
                    return res.status(404).send({
                        message: 'User not have order'
                    })
                }
                return order
                    .update({
                        notes: req.body.notes || order.notes,
                        address: req.body.address || order.address,
                        mobile: req.body.mobile || order.mobile,
                        mobile2: req.body.mobile2 || order.mobile2,
                        cash_type: req.body.cash_type || order.cash_type,
                        total: req.body.total || order.total,
                        sub_total:req.body.sub_total || order.sub_total,
                        tax: req.body.tax || order.tax,
                        first_name: req.body.first_name ||order.first_name,
                        last_name: req.body.last_name || order.last_name,
                        ship: req.body.ship || order.ship,
                        trackingNumber: req.body.trackingNumber || order.trackingNumber,
                        status: req.body.status || order.status,
                        paymentId: req.body.paymentId || order.paymentId,
                        userId: req.params.userId || order.userId
                    })
                    .then(() =>res.status(201).send(order))
                    .catch(error =>res.status(500).send(error));
            })
            .catch(error =>res.status(500).send(error));

    }
};
