/**
 * Created by hoangdo on 9/15/2017.
 */

const User = require('../../models').user;
const Contact = require('../../models').contact;
var jwt = require('jsonwebtoken');
var    bcrypt = require('bcrypt');

module.exports = {
    // create(req,res){
    //     return Contact
    //         .create({
    //             email: req.body.email,
    //             email2: req.body.email2,
    //             name: req.body.name,
    //             name2: req.body.name2,
    //             hotline: req.body.hotline,
    //             office_phone: req.body.office_phone,
    //             office_phone2: req.body.office_phone2
    //         })
    //         .then(contact =>{
    //             return User
    //                 .create({
    //                     username: req.body.username,
    //                     password : req.body.password,
    //                     googleID: req.body.googleID,
    //                     facebookID: req.body.facebookID,
    //                     avatar: req.body.avatar,
    //                     type: req.body.type,
    //                     role: req.body.role,
    //                     contactId: contact.id
    //                 })
    //                 .then(user => res.status(201).send(user))
    //                 .catch(error => res.status(500).send(error));
    //         })
    //         .catch(error => res.status(500).send(error));
    // },
    // list(req, res){
    //     return User
    //         .findAll({
    //             include: {
    //                 all: true
    //             }
    //         })
    //         .then(user => res.status(201).send(user))
    //         .catch(error => res.status(500).send(error));
    // },
    // retrieve(req, res){
    //     return User
    //         .findById(req.params.userId ,{
    //             include: {
    //                 all:true
    //             }
    //         })
    //         .then(user => {
    //             if(!user){
    //                 return res.status(404).send({
    //                     message: 'User not found'
    //                 })
    //             }
    //             return res.status(201).send(user);
    //         })
    //
    //         .catch(error => res.status(500).send(error));
    // }

    // create(req,res){
    //     return User
    //         .create({
    //             username: req.body.username,
    //             password: req.body.password
    //         })
    //         .then(user =>{
    //             return User
    //             if(user.username !== User.username || user.password !== User.password){
    //                 res.status(404).send({message : 'user or password not found'})
    //             }else {
    //                 res.status(201).send({mesage: 'Login  Success'})
    //             }
    //         })
    //         .catch(error=>res.status(500).send(error))
    // }
    register(req, res){
        // let newUser = new User(req.body);
        // newUser.password = bcrypt.hashSync(req.body.password, 10);
        // newUser.save(function(err, user) {
        //     if (err) {
        //         return res.status(400).send({
        //             message: err
        //         });
        //     } else {
        //         user.password = undefined;
        //         return res.json(user);
        //     }
        // });
        var hashedPassword = bcrypt.hashSync(req.body.password, 8);
        return User
            .create({
                username: req.body.username,
                password: hashedPassword,
                googleID: req.body.googleID,
                facebookID: req.body.facebookID,
                avatar: req.body.avatar,
                type: req.body.type,
                role: req.body.role
            })
            .then( user => res.status(201).send(user))
            .catch(error => res.status(500).send(error));
    },
    sing_in(req,res){
        // return User
        //     .findOne({
        //     where: {username: req.body.username}
        // }, function(err, user) {
        //     if (err) throw err;
        //     if (!user) {
        //         res.status(401).json({ message: 'Authentication failed. User not found.' });
        //     } else if (user) {
        //         if (!user.comparePassword(req.body.password)) {
        //             res.status(401).json({ message: 'Authentication failed. Wrong password.' });
        //         } else {
        //             return res.json({token: jwt.sign({ username: user.username, googleID: user.googleID, id: user.id , facebookID: user.facebookID ,avatar: user.avatar ,type: user.type , role: user.role }, 'RESTFULAPIs')});
        //         }
        //     }
        // });
        return User
            .findOne({
            where: {username: req.body.username}
        })
            .then(user  =>{

                if(!user){
                    res.status(404).send({mesage: 'not found'})
                }else {
                    if(!user.comparePassword(req.body.password)){
                        res.status(401).send({ message: 'Authentication failed. Wrong password.' });
                    }else {
                        return res.status(200).send({token: jwt.sign({ username: user.username, googleID: user.googleID, id: user.id , facebookID: user.facebookID ,avatar: user.avatar ,type: user.type , role: user.role }, 'RESTFULAPIs')});
                    }
                }

            })
            .catch(error => res.status(500).send(error));
    },
    loginRequired(req, res , next){
        if (req.user) {
            next();
        } else {
            return res.status(401).json({ message: 'Unauthorized user!' });
        }
    }

};