import express from 'express';
var router = express.Router();
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
// import {User} from "../../../dist/models/user";
const User = require('../../models').user;
var config = require('../../../dist/app');
router.use(bodyParser.urlencoded({extended :false}));
router.use(bodyParser.json());


module.exports ={
    create(req, res){
        var hashedPassword = bcrypt.hashSync(req.body.password, 8);
        return User
            .create({
                username: req.body.username,
                password: hashedPassword,
                googleID: req.body.googleID,
                facebookID: req.body.facebookID,
                avatar: req.body.avatar,
                type: req.body.type,
                role: req.body.role
            })
            .then( user => {




                var token = jwt.sign({id: user.id} , config.secret ,{expiresIn : 86400});
                res.status(200).send({auth: true , token: token} )
            })
            .catch(error => res.status(500).send(error));
    }
};
// router.post('/api/user/register', function(req, res) {
//
//     var hashedPassword = bcrypt.hashSync(req.body.password, 8);
//
//     User.create({
//             username: req.body.username,
//                 password: hashedPassword,
//                 googleID: req.body.googleID,
//                 facebookID: req.body.facebookID,
//                 avatar: req.body.avatar,
//                 type: req.body.type,
//                 role: req.body.role
//         },
//         function (err, user) {
//             if (err) return res.status(500).send("There was a problem registering the user.")
//             // create a token
//             var token = jwt.sign({ id: user.id }, config.secret, {
//                 expiresIn: 86400 // expires in 24 hours
//             });
//             res.status(200).send({ auth: true, token: token });
//         });
// });