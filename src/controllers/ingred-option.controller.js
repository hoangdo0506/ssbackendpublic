/**
 * Created by hoangdo on 9/15/2017.
 */

const IngredOption = require('../models').ingred_option;

module.exports ={
    create(req, res){
        return IngredOption
            .create({
                photo : req.body.photo,
                description: req.body.description,
                order_count: req.body.order_count,
                ingredientId: req.body.ingredientId
            })
            .then(ingredOption => res.status(200).send(ingredOption))
            .catch(error => res.status(500).send(error));
    },
    update(req,res){
        return IngredOption
            .find({
                where: {
                    id: req.params.ingredOptionId,
                    ingredientId: req.params.ingredientId
                }
            })
            .then(ingredOption =>{
                if(!ingredOption){
                    return res.status(404).send({
                        message: 'IngredOption not found'
                    })
                }
                return ingredOption
                    .create({
                        photo : req.body.photo,
                        description: req.body.description,
                        order_count: req.body.order_count
                    })
                    .then(ingredOption => res.status(200).send(ingredOption))
                    .catch(error => res.status(500).send(error));
            })
            .catch(error => res.status(500).send(error));
    },
    destroy(req, res){
        return IngredOption
            .find({
                where: req.params.ingredOptionId,
                ingredientId: req.params.ingredientId,
            })
            .then(ingredOption =>{
                if(!ingredOption){
                    return res.status(400).send({
                        message: 'Ingred option not found'
                    });
                }
                return ingredOption
                    .destroy()
                    .then(()=>res.status(200).send({
                        message: 'Ingred option delete successfully'
                    }))
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => res.status(500).send(error));
    }

};