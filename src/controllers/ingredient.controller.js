/**
 * Created by hoangdo on 9/14/2017.
 */

const Ingredient = require('../models').ingredient;
const  IngredOption = require('../models').ingred_option;

module.exports ={
    create(req ,res){
        return Ingredient
            .create({
                name: req.body.name,
                photo: req.body.photo,
                description: req.body.description,
                source_type: req.body.source_type,
                process_type: req.body.process_type,
                status: req.body.status,
                type: req.body.type
            })
            .then(ingredient => res.status(201).send(ingredient))
            .catch(error => res.status(404).send(error));
    },

    list(req,res){
        return Ingredient
            .findAll({
                include : [{
                    model: IngredOption,
                   as: 'ingred_options'
                }],
            })
            .then(ingredient => res.status(201).send(ingredient))
            .catch(error => res.status(400).send(error));
    },
    retrieve(req,res){
        return Ingredient
            .findById(req.params.ingredientId ,{

                include: [{
                    model: IngredOption,
                    as: 'ingred_options'
                }],
            })
            .then(ingredment =>{
                if(!ingredment){
                    return res.status(404).send({
                        message: 'Ingred not found'
                    });
                }
                return res.status(200).send(ingredment);
            })
            .catch(error => res.status(400).send(error));
    },
    update(req,res){
        return Ingredient
            .findById(req.params.ingredientId , {
                include: [{
                    model: IngredOption,
                    as: 'ingred_options'
                }]
            })
            .then(ingredient => {
                if(!ingredient){
                    return res.status(404).send({
                        message: 'Ingredient not found'
                    });
                }
                return ingredient
                    .update({
                        name: req.body.name || ingredient.name,
                        photo: req.body.photo || ingredient.photo,
                        description: req.body.description || ingredient.description,
                        source_type: req.body.source_type || ingredient.source_type,
                        process_type: req.body.process_type || ingredient.proces_type,
                        status: req.body.status || ingredient.status,
                        type: req.body.type || ingredient.type
                    })
                    .then(()=> res.status(201).send(ingredient))
                    .catch(error => res.status(500).send(error));
            })
            .catch(error => res.status(500).send(error));
    },
    destroy(req, res){
        return Ingredient
            .findById(req.params.ingredientId)
            .then(ingredient => {
                if(!ingredient){
                    return res.status(404).send({
                        message: 'Ingredient Not found'
                    });

                }
                return ingredient
                    .destroy()
                    .then(() => res.status(201).send({
                        message: 'Ingredient Delete Succesfully'
                    }))
                    .catch(error => res.status(500).send(error));
            })
            .catch(error => res.status(500).send(error));
    }
};