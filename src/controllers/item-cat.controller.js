/**
 * Created by hoangdo on 9/9/2017.
 */
const ItemCat = require('../models').item_cat;

module.exports = {
    getAll(req, res) {
        return ItemCat
            .all()
            .then(cats => res.status(200).send(cats))
            .catch(err => res.status(400).send(err))
    },

    create(req, res) {
        return ItemCat
            .create({
                name : req.body.name,
                photo: req.body.photo
            })
            .then(itemcat => res.status(201).send(itemcat))
            .catch(error => res.status(400).send(error));
    }
};