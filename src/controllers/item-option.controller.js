/**
 * Created by hoangdo on 9/12/2017.
 */
const ItemOption = require('../models').item_option;

module.exports ={
    create(req, res){
        return ItemOption
            .create({
                description: req.body.description,
                type: req.body.type,
                order_count: req.body.order_count,
                status: req.body.status,
                itemId: req.body.itemId,

            })
            .then(itemOption => res.status(201).send(itemOption))
            .catch(error => res.status(400).send(error));
    },
    update(req , res){
        return ItemOption
            .find({
                where: {
                    id: req.params.itemOptionId,
                    itemId: req.params.itemId,
                },
            })
            .then(itemOption =>{
                if(!itemOption){
                    return res.status(404).send({
                        message : 'Item option not found'
                    });
                }

                return itemOption
                    .update({
                        description: req.body.description || itemOption.description,
                        type: req.body.type || itemOption.type,
                        order_count: req.body.order_count || itemOption.order_count,
                        status: req.body.status || itemOption.status
                    })
                    .then(()=> res.status(200).send(itemOption))
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => res.status(500).send(error));
    },

    destroy(req, res){
        return ItemOption
            .find({
                where: req.params.itemOptionId,
                itemId: req.params.itemId,
            })
            .then(itemOption =>{
              if(!itemOption){
                  return res.status(400).send({
                      message: 'Item option not found'
                  });
              }
              return itemOption
                  .destroy()
                  .then(()=>res.status(200).send({
                      message: 'Item option delete successfully'
                  }))
                  .catch(error => res.status(400).send(error));
            })
            .catch(error => res.status(500).send(error));
    }
    
}