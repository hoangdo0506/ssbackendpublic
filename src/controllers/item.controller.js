/**
 * Created by hoangdo on 9/10/2017.
 */
const Item = require('../models').item;
const ItemOption =require('../models').item_option;

module.exports = {
    create(req,res){
        return Item
            .create({
            name: req.body.name,
                description : req.body.description,
                photo : req.body.photo,
                photo2: req.body.photo2,
                photo3:req.body.photo3,
                photo4: req.body.photo4,
                photo5: req.body.photo5,
                photo6: req.body.photo6,
                amount: req.body.amount,
                unit: req.body.unit,
                status: req.body.status,

        })
            .then(item => res.status(201).send(item))
            .catch(error => res.status(400).send(error));
    },

    list(req,res){
        return Item
            // .findAll({
            //     include: [{
            //         model: ItemOption,
            //         as: 'item_options'
            //     }],
            // })
            .all()
            .then(item =>res.status(201).send(item))
            .catch(err =>res.status(400).send(err));
    },
    retrieve(req,res){
        return Item
            .findById(req.params.itemId ,{

                include: [{
                    model: ItemOption,
                    as: 'item_options'
                }],
            })
            .then(item =>{
                if(!item){
                    return res.status(404).send({
                        message: 'Item not found'
                    });
                }
                return res.status(200).send(item);
            })
            .catch(error => res.status(400).send(error));
    },
    update(req, res){
        return Item
            .findById(req.params.itemId ,{
                include: [{
                    model: ItemOption,
                    as: 'item_options',
                }],
            })
            .then(item =>{
                if(!item){
                    return res.status(404).send({
                        message: 'Item Not Found'
                    });
                }
                return item
                    .update({
                        name: req.body.name || item.name ,
                        description: req.body.description  || item.description,
                        photo : req.body.photo || item.photo,
                        photo2: req.body.photo2 || item.photo2,
                        photo3: req.body.photo3 || item.photo3,
                        photo4: req.body.photo4 || item.photo4,
                        photo5: req.body.photo5 || item.photo5,
                        photo6: req.body.photo6  || item.photo6,
                        amount: req.body.amount || item.amount,
                        unit: req.body.unit || item.unit,
                        status: req.body.status || item.status,

                    })
                    .then(() => res.status(200).send(item))
                    .catch(error => res.status(400).send(error));
            })
            .catch(error =>res.status(400).send(error));
    },
    destroy(req, res){
        return Item
            .findById(req.params.itemId)
            .then(item => {
                if(!item){
                    return res.status(400).send({
                        message: 'Item not found'
                    });
                }
                return item
                    .destroy()
                    .then(()=> res.status(204).send({
                        message: 'Item Delete Successfully.'
                    }))
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => res.status(400).send(error));

    },
};