'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('item_options', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.TEXT
      },
      type: {
        type: Sequelize.STRING
      },
      order_count: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        itemId:{
            type : Sequelize.INTEGER,
            onDelete: 'CASCADE',
            references: {
                model:'items',
                key: 'id',
                as: 'itemId'
            },
        },
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('item_options');
  }
};