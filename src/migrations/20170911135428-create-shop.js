'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('shops', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        contactId: {
        type: Sequelize.INTEGER,
            references: {
          model: 'contacts',
                key: 'id',
                as: 'contactId'
            }
        }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('shops');
  }
};