'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('suppliers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      compID: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        contactId:{
        type: Sequelize.INTEGER,
            references:{
          model: 'contacts',
                key: 'id',
                as: 'contactId'
            }
        }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('suppliers');
  }
};