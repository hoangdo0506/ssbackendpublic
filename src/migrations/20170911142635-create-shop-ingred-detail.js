'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('shop_ingred_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        shopId: {
        type : Sequelize.INTEGER,
            references: {
          model: 'shops',
                key: 'id',
                as: 'shopId'
            },
        },
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('shop_ingred_details');
  }
};