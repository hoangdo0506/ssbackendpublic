'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('ingredCat_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
        ingredCatId: {
        type: Sequelize.INTEGER,
            references: {
          model: 'ingred_cats',
                key: 'id',
                as: 'ingredCatId'
            },
        },
        ingredientId:{
        type: Sequelize.INTEGER,
            references:{
          model: 'ingredients',
                key: 'id',
                as: 'ingredientId'
            },
        }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('ingredCat_details');
  }
};