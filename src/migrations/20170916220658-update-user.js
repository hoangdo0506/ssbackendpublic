'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      return [
          queryInterface.addColumn('users' , 'contactId' ,{
              type: Sequelize.INTEGER,
              references: {
                  model: 'contacts',
                  key: 'id',
                  as: 'contactId'
              }
          })
      ];
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
