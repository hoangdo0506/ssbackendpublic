'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      return [
          queryInterface.addColumn('contacts' , 'latitude' ,{
              type: Sequelize.DECIMAL(9,6)
          }),
          queryInterface.addColumn('contacts' , 'longitude' ,{
              type: Sequelize.DECIMAL(9,6)
          })
      ];
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.dropTable('contacts');
  }
};
