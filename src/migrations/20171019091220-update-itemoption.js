'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return [
          queryInterface.addColumn('item_options' , 'ingedList' ,{
              type: Sequelize.ARRAY(Sequelize.INTEGER)
          }),
          queryInterface.addColumn('item_options' , 'ingredOpList' ,{
              type: Sequelize.ARRAY(Sequelize.INTEGER),
              get: function() {
                  return JSON.parse(this.getDataValue('ingredOpList'));
              },
              set: function(val) {
                  return this.setDataValue('ingredOpList', JSON.stringify(val));
              }
          })
      ];
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
