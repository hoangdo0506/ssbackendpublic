'use strict';
module.exports = function(sequelize, DataTypes) {
  var ingred_option = sequelize.define('ingred_option', {
    photo: DataTypes.STRING,
    description: DataTypes.TEXT,
    order_count: DataTypes.INTEGER
  });
  ingred_option.associate =(models) =>{
    ingred_option.hasMany(models.item_op_ingred_detail ,{
      foreignKey: 'ingredOptionId',
        as: 'item_op_ingred_details'
    });
    ingred_option.belongsTo(models.ingredient ,{
      foreignKey: 'ingredientId'
    });
    ingred_option.hasMany(models.supplier_ingred_op_detail , {
      foreignKey: 'ingredOptionId',
        as: 'supplier_ingred_op_details'
    });
  };
  return ingred_option;
};