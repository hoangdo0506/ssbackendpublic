'use strict';
module.exports = function(sequelize, DataTypes) {
  var ingredCat_detail = sequelize.define('ingredCat_detail', {
  });
  ingredCat_detail.associate =(models) =>{
    ingredCat_detail.belongsTo(models.ingredient ,{
      foreignKey: 'ingredientId',
        onDelete: 'CASCADE'
    });
    ingredCat_detail.belongsTo(models.ingred_cat ,{
      foreignKey: 'ingredCatId',
        onDelete: 'CASCADE'
    })
  };
  return ingredCat_detail;
};