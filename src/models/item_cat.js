'use strict';
module.exports = function(sequelize, DataTypes) {
  var item_cat = sequelize.define('item_cat', {
    name: DataTypes.STRING,
    photo: DataTypes.STRING
  });
    item_cat.associate = (models)=>{
        item_cat.hasMany(models.itemCat_detail ,{
            foreignKey: 'itemCatId',
            as: 'itemCat_details'
        });
        item_cat.belongsTo(models.item ,{
            foreignKey: 'itemId',
            onDelete: 'CASCADE'
        });
    };
  return item_cat;
};