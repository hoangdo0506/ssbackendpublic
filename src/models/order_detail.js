'use strict';
module.exports = function(sequelize, DataTypes) {
  var order_detail = sequelize.define('order_detail', {
    quantity: DataTypes.DECIMAL,
    sum: DataTypes.DECIMAL,
    image: DataTypes.STRING,
    name: DataTypes.STRING,
    code: DataTypes.STRING
  });
    order_detail.associate =(models)=>{
        order_detail.belongsTo(models.order,{
            foreignKey: 'orderId',
            onDelete: 'CASCADE'
        });
        order_detail.belongsTo(models.shop_item_detail,{
            foreignKey: 'shopItemId',
            onDelete: 'CASCADE'
        });
    };
  return order_detail;
};