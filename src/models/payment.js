'use strict';
module.exports = function(sequelize, DataTypes) {
  var payment = sequelize.define('payment', {
    status: DataTypes.STRING,
    creditCardName: DataTypes.STRING,
    creditCardCode: DataTypes.STRING,
    creditCardType: DataTypes.STRING,
    creditCardDate: DataTypes.DATE,
    value: DataTypes.DECIMAL,
    unit: DataTypes.STRING
  });
  payment.associate = (models)=>{
    payment.hasOne(models.order,{
      foreignKey: 'paymentId',
        as: 'orders'
    });
  };
  return payment;
};