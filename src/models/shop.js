'use strict';
module.exports = function(sequelize, DataTypes) {
  var shop = sequelize.define('shop', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT
  });
  shop.associate =(models) =>{
    shop.belongsTo(models.contact ,{
      foreignKey: 'contactId'
    });
    shop.hasMany(models.shop_item_detail,{
      foreignKey: 'shopId',
        as: 'shop_item_details'
    });
    shop.hasMany(models.shop_ingred_detail,{
      foreignKey: 'shopId',
        as: 'shop_ingred_details'
    });
  };
  return shop;
};