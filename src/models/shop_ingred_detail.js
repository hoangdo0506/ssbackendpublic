'use strict';
module.exports = function(sequelize, DataTypes) {
  var shop_ingred_detail = sequelize.define('shop_ingred_detail', {
    status: DataTypes.STRING
  });
  shop_ingred_detail.associate =(models) =>{
    shop_ingred_detail.belongsTo(models.shop , {
      foreignKey: 'shopId',
        onDelete: 'CASCADE'
    });
  };
  return shop_ingred_detail;
};