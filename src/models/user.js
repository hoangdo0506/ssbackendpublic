'use strict';
var bcrypt = require('bcrypt');
module.exports = function(sequelize, DataTypes) {
  var user = sequelize.define('user', {
      username: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
          validate: {
              is: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          }
      },
    password: DataTypes.STRING,
    googleID: DataTypes.STRING,
    facebookID: DataTypes.STRING,
    avatar: DataTypes.STRING,
    type: DataTypes.STRING,
    role: DataTypes.STRING
  });

      user.associate = (models)=>{
          user.hasMany(models.order,{
              foreignKey: 'userId',
              as: 'orders'
          });
          user.belongsTo(models.contact ,{
              foreignKey: 'contactId'
          });
      };
      user.prototype.comparePassword = function(password) {
          return bcrypt.compareSync(password, this.password);
      }
  return user;
};